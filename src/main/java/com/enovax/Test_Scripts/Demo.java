package com.enovax.Test_Scripts;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.time.DurationFormatUtils;
//import org.apache.poi.ss.formula.functions.Column;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.enovax.Test_Scripts.Application_TestCases.ClsTestCases;
import com.enovax.Test_Scripts.Application_Utilities.Application_Utilities;
import com.enovax.Test_Scripts.Application_Utilities.ExcelLib;

import API_Implementation.API;
import Utills.DomainList;
import Utills.ExcelUtils;
import Utills.Report;
import io.appium.java_client.AppiumDriver;

public class Demo {
	
	private WebDriver driver;
	private String URL;
	private long start;
	/*String firefox;
	String chrome;
	String safari;*/
	String waitingTime;	
	String browsername;
	String prevBrowserName;
	int iResultColumn = 3;
	final String PASS_KEYWORD = "Pass", FAIL_KEYWORD = "Fail"; 
	
	@BeforeClass
	public void setupSelenium() throws Exception {
		waitingTime=AppDriver.getWaitingTime();
		/*chrome=Webdriver.getchrome();
		firefox=Webdriver.getfirefox();
		safari = Webdriver.getSafari();*/
	}

	@BeforeMethod
	public void beforeTest() throws InterruptedException{
		start = Reporter.getCurrentTestResult().getStartMillis();
	}
	
	@AfterMethod
	public void afterTest(){
		long end = Reporter.getCurrentTestResult().getEndMillis();
		String execTime =DurationFormatUtils.formatDurationHMS((end-start));
		System.out.println("Total Test Execution time - millisec: " + execTime);
		System.out.println("Total Test Execution time - HMS: " + DurationFormatUtils.formatDurationHMS((end-start)));
	}
	
		
	@Test(dataProviderClass=Utills.DomainDataProvider.class,dataProvider="DomainListData")
    public void fnTest(DomainList domainListItem) throws UnknownHostException, MalformedURLException, InterruptedException {
		
		browsername = domainListItem.getBrowserName();
		//URL= domainListItem.getDomainName();
		AppDriver.createWebDriverObject(browsername);
		driver=AppDriver.getDriverObject();
		API API = PageFactory.initElements(driver, API.class);
		//API.launchClientURL(driver, URL, browsername);
		
		Report.screenShotFolder = AppDriver.screenShotFolder;
		Report.LogHTMLFolder = AppDriver.LogHTMLFolder;
		Report.ReportHeader("TA sample script");
		Global_Variable.iTcCnt = 0;
		Global_Variable.iTcCnt++;
		
		try {
			System.out.println("Test"+Global_Variable.TC_TD_Path);
			ExcelLib.setExcelFile(Global_Variable.TC_TD_Path);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try{
			
			System.out.println("Main script");
			
		}catch(Exception e){
			Report.Fail(driver,"Exception" + e.getMessage());
			e.printStackTrace();
		}finally{
			Report.writeToLog(URL);
		}
	
		}
	
	
	
}

