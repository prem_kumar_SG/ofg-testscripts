package com.enovax.Test_Scripts.Application_Utilities;

/*

S.No  Class name		Created by	Created Date	Purpose			Modifed By	Modified Date
**************************************************************************************************
1. 	   ExcelUtils		Prem kumar	24/May/2017		To handle Excel	
**/

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelLib {
	
	public static XSSFWorkbook ExcelBook;
	public static XSSFSheet ExcelSheet;
	public static XSSFCell Cell;
	public static XSSFRow Row;
	
	/**
	 Method name : setExcelFile
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To set the File path and to open the Excel file
	 Modified By :
	 Modified Date :
	 */
    public static void setExcelFile(String File) throws Exception {
    	   try {
				FileInputStream ExcelFile = new FileInputStream(File);
				ExcelBook = new XSSFWorkbook(ExcelFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
    }
    
    /**
	 Method name : getCellData
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To read the test data from the Excel cell
	 Modified By :
	 Modified Date :
	 */
    public static String getCellData(int RowNum, int ColNum, String SheetName ) throws Exception{
    	 ExcelSheet = ExcelBook.getSheet(SheetName);
		 try{
		         String CellData = ExcelSheet.getRow(RowNum).getCell(ColNum).getStringCellValue();
		         return CellData;
	         }catch (Exception e){
	           return null;
	         }
    	}
	
    /**
	 Method name : getRowCount
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To get the row count used of the excel sheet
	 Modified By :
	 Modified Date :
	 */
	public static int getRowCount(String SheetName){
		int RowNum = 0;
		try {
			ExcelSheet = ExcelBook.getSheet(SheetName);
			RowNum=ExcelSheet.getLastRowNum()+1;
			return RowNum;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RowNum;
	}
	
	/**
	 Method name : getRowContains
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To get the Row number of the test case
	 Modified By :
	 Modified Date :
	 */
	public static int getRowContains(String sTestCaseName, int colNum,String SheetName) throws Exception{
		int i;	
		ExcelSheet = ExcelBook.getSheet(SheetName);
		int rowCount = ExcelLib.getRowCount(SheetName);
		for (i=0 ; i<rowCount; i++){
			if  (ExcelLib.getCellData(i,colNum,SheetName).equalsIgnoreCase(sTestCaseName)){
				break;
			}
		}
		return i;
	}
	
	/**
	 Method name : getRowContains
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To set the value into the cell
	 Modified By :
	 Modified Date :
	 */
	@SuppressWarnings({ "static-access", "deprecation" })
	public static void setCellData(String File,String SheetName,  int RowNum, int ColNum, String Result) throws Exception    {
        try{

        	ExcelSheet = ExcelBook.getSheet(SheetName);
            Row  = ExcelSheet.getRow(RowNum);
            Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
            if (Cell == null) {
         	   Cell = Row.createCell(ColNum);
         	   Cell.setCellValue(Result);
             } else {
                 Cell.setCellValue(Result);
             }
              FileOutputStream fileOut = new FileOutputStream(File);
              ExcelBook.write(fileOut);
              fileOut.flush();
              fileOut.close();
              ExcelBook = new XSSFWorkbook(new FileInputStream(File));
          }catch(Exception e){
        	  e.printStackTrace();
          }
     }
	
}
