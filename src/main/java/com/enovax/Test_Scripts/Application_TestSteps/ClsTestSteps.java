/*
	
	S.No  Class name		Created by	Created Date	Purpose										Modifed By	Modified Date
	***************************************************************************************************************************
	  1	  ClsTestSteps		Prem Kumar	25/May/2017		To maintain test steps for all Test cases
	**/
package com.enovax.Test_Scripts.Application_TestSteps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.enovax.Test_Scripts.AppDriver;

import API_Implementation.API;
import ObjectRepository.OR;

public class ClsTestSteps {
	private static  API API;
	private static OR OR;
	private static WebDriver driver;
	
	
	public static boolean fnUsername(String username)
	{
		driver = AppDriver.getDriverObject();
		API = PageFactory.initElements(driver, API.class);
		OR = PageFactory.initElements(driver,OR.class);
		
		return API.EnterTextFeild(driver, OR.txtUSername, "Username textbox", username);
	}
	
	public static boolean fnPassword(String password)
	{
		driver = AppDriver.getDriverObject();
		API = PageFactory.initElements(driver, API.class);
		OR = PageFactory.initElements(driver,OR.class);
		
		return API.EnterTextFeild(driver, OR.txtPassword, "Passwod textbox", password);
	}
	
	public static boolean fnclickLogin()
	{
		driver = AppDriver.getDriverObject();
		API = PageFactory.initElements(driver, API.class);
		OR = PageFactory.initElements(driver,OR.class);
		
		return API.ClickFeild(driver, OR.btnLogin, "Login button");
	}
	
}
