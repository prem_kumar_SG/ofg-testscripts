package com.enovax.Test_Scripts;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.xml.XmlSuite;

import Utills.ExcelUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class AppDriver{
	private static String waitingTime;
	private static String browser;
	/*private static String chrome;
	private static String firefox;
	private static String safari;
	private static String InternetExplorer;*/
	private String project;
	public static  String testLogHTMLLocation;
	public String testLogHTMLFolderName;
	public String testLogScreenShotFolderName;
	public String testLogFileName;
	public String ip;
	public static  File testlogFolder;
	public static File LogHTMLFolder;
	public static File screenShotFolder;
	private static AppiumDriver driver ;
	static String domainsFilePath;
	static String staging;
	int proj;
	static long start;
	static String timeStamp;
	static String Env;
	
	
	@BeforeSuite
	public void beforeSuite(ITestContext context) throws UnknownHostException, MalformedURLException{
		try {
			start = Reporter.getCurrentTestResult().getStartMillis();
			System.out.println("Test Suite Started....");
			waitingTime = context.getCurrentXmlTest().getParameter("waitingTime");
			project = context.getCurrentXmlTest().getParameter("project");
			
			//firefox = context.getCurrentXmlTest().getParameter("firefoxProfilePath");
			domainsFilePath = context.getCurrentXmlTest().getParameter("domainsFilePath");
			String environmentSheetName = context.getCurrentXmlTest().getParameter("environmentSheetName");
			ExcelUtils readFromExcel=new ExcelUtils(domainsFilePath);
			staging  = setEnvironmentDetails(readFromExcel,environmentSheetName);
			setBrowserDetails(readFromExcel,environmentSheetName);
			testLogHTMLLocation = context.getCurrentXmlTest().getParameter("testReportHTMLLogPath");
			testLogHTMLFolderName = context.getCurrentXmlTest().getParameter("testReportHTMLFolderName");
			testLogScreenShotFolderName = context.getCurrentXmlTest().getParameter("testReportScreenShotFolderName");
//			chrome = context.getCurrentXmlTest().getParameter("chromeDriver");
//			InternetExplorer = context.getCurrentXmlTest().getParameter("iedriver");
			timeStamp = new SimpleDateFormat("_yy-MMM-dd-HH-mm").format(Calendar.getInstance().getTime());
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}	
		
		try{
			testLogFileName="Enovax_Suite"+timeStamp;
			
			if(staging.equals("Staging")){
				testLogHTMLLocation = testLogHTMLLocation.concat("/").concat("STAGING").concat("/").concat(testLogFileName);
				
			}	
			if(staging.equals("Production")){
				testLogHTMLLocation = testLogHTMLLocation.concat("/").concat("PRODUCTION").concat("/").concat(testLogFileName);
			}	
			if(staging.equals("QA")){
				testLogHTMLLocation = testLogHTMLLocation.concat("/").concat("QA").concat("/").concat(testLogFileName);
			}
			
			testlogFolder = new File(testLogHTMLLocation);

			LogHTMLFolder = new File(testlogFolder.getAbsolutePath().concat("/").concat(testLogHTMLFolderName));
						
		    screenShotFolder = new File(LogHTMLFolder.getAbsolutePath().concat("/").concat(testLogScreenShotFolderName));

		}catch(Exception e){
			Reporter.log("Log Files Error : "+e);
		}finally{
		}
	}
	
	@AfterSuite
	public void afterSuite() throws Exception{
		try {
			long end = Reporter.getCurrentTestResult().getEndMillis();
			String totalTime = DurationFormatUtils.formatDuration((end-start), "HH:mm:ss,SSS");
			System.out.println("Total Suite Execution time(HMS): " + totalTime);
			driver.quit();
			XmlSuite suite = new XmlSuite();
			suite.setName("Enovax suite");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//Create WebDriver Browser instance
	public static void createWebDriverObject(String browser) throws MalformedURLException, InterruptedException{
		try{
			

			//**Configuration start**
			
			// Create object of DesiredCapabilities class   
			DesiredCapabilities capabilities = new DesiredCapabilities();
			// Specify the device name (any name)
			capabilities.setCapability("deviceName", "Nexus tablet");
			// Platform version
			capabilities.setCapability("platformVersion", "4.4");
			// platform name
			capabilities.setCapability("platformName", "Android");
			driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			
			// Specify the implicit wait of 5 second
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			//**Configuration end**
			
				System.out.println("Driver Object is ready");
			}catch(Exception e){
				System.out.println(e);
			}
		}	
	
	//Return AppiumDriver Object
	public static WebDriver getDriverObject(){
		return driver;
	}
	
	public static String getInputDomainPath(){
		return domainsFilePath;
	}
	
	public static String getTestLogHTMLLocation(){
		return testLogHTMLLocation;
	}
	
	public static String getWaitingTime(){
		return waitingTime;
	}
	
/*	public static String getfirefox(){
		return firefox;
	}
	
	public static String getchrome(){
		return chrome;
	}
	
	public static String getSafari(){
		return safari;
	}*/
	
	public static File getTestlogFolder(){
		return testlogFolder;
	}
	
	public static File getClientLogHTMLFolder(){
		return LogHTMLFolder;
	}
	
	public static File getScreenShotFolder(){
		return screenShotFolder;
	}
	
	public static String setEnvironmentDetails(ExcelUtils readFromExcel,String sheetName){
    	String selection=null;
    	try{
    		readFromExcel.setSheet(sheetName);
    		selection =  readFromExcel.getCellValue("B4");
    		Env=readFromExcel.getCellValue("B4");
    	}catch(Exception e){
    		readFromExcel.close();	
    	}
    	
    	return Env;
     }
	
	public static void setBrowserDetails(ExcelUtils readFromExcel,String sheetName){
    	
    	try{
    		readFromExcel.setSheet(sheetName);
    		browser=readFromExcel.getCellValue("B5");
    	}catch(Exception e){
    		readFromExcel.close();	
    	}
     }
}
